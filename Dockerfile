# Imagen Multi-stage, solo la última imagen es la que cuenta
# Los comandos de las imagenes anteriores on pueden verse con: docker history IMAGE_ID
# Ayuda a reducir el tamaño de las imagenes finales y a impedir que queden trazas de posibles valores secretos (llaves ssh)

# Build environment
#------------------

FROM node:12.2.0-alpine as build

# valor por defecto para la variable de entorno API_URL, se puede sobreescribir por linea de comandos al construir la imagen
# docker build --build-arg REACT_APP_API_URL=valor ... the rest of the build command is omitted
ARG REACT_APP_API_URL=https://api.roberman.diskstation.me
# variable de entorno para React
ENV REACT_APP_API_URL=${REACT_APP_API_URL}

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install
RUN npm install react-scripts@3.0.1 -g
COPY . /app
RUN npm run build

# Production environment
#-----------------------

FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]