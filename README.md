## Variables de configuración

En el fichero .env hay que ajustar la variable `REACT_APP_API_URL` con la URL del api

## Desplegar en NAS Synology

1. Eliminar los posibles contenedores/imagenes anteriores en docker (esto se puede hacer facilmente desde DSM).
2. Entrar por SSH al NAS.
3. Ejecutar el comando `sudo -i` para obtener permisos root.
3. Ir a la carpeta del proyecto (/volume1/repos/movies_react).
4. Ejecutar el comando `docker build -t movies-frontend .` para construir la imagen.
5. Desde DSM, arrancar un contenedor de la nueva imagen creada. Mapear el puerto local 8001 al puerto 80 del contenedor
para que el proxy inverso del NAS acceda al contenedor correctamente.

Nota - el Dockerfile actual genera un contenedor para compilar el proyecto y al terminar lo elimina,
pero deja una imagen en el registro docker
1. Con `docker images` podemos ver esas imágenes con nombre `<none>`. Estas imágenes no puede verse desde DSM.
2. Podemos eliminar esas imágenes con `docker imagen rm xxxxxx` donde xxxxxx es IMAGE ID listado por `docker images`.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.