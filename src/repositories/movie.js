export default class MovieRepository {

    getFetch(url)
    {
        let urlCompleta = process.env.REACT_APP_API_URL + url;
        console.log('getFetch: ' + urlCompleta);
        return fetch(urlCompleta).then(data => data.json());
    }

    getDataFromDbRecientes() {
        return new Promise((resolve) => {
            this.getFetch('/api/getpeliculasrecientes')
                .then(json => resolve(json));
        });
    }

    getDataFromDbPeliculas() {
        return new Promise((resolve) => {
            this.getFetch('/api/getpeliculas')
                .then(json => resolve(json));
        });
    }

    getDataFromDbPeliculasFiltro(titulo) {
        return new Promise((resolve) => {
            this.getFetch('/api/getpeliculas/' + titulo)
                .then(json => resolve(json));
        });
    }

    getStats() {
        return new Promise((resolve) => {
            this.getFetch('/api/getstats')
                .then(json => resolve(json));
        });
    }
}