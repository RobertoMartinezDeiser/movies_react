import React, { Component } from 'react';

//https://github.com/midudev/aprende-react-js/tree/master/sesion-03/ejemplos/src

export default class CicloVidaDemo extends Component {

    // Constructor
    constructor (...args) {
        super(...args);

        this.changeState = this.changeState.bind(this);

        this.state = {
            mensaje: 'Mensaje inicial'
        }

        console.log('<CicloVidaDemo> Constructor');
    }

    // Metodos de ciclo de vida
    componentWillMount () {
        console.log('<CicloVidaDemo> componentWillMount');
    }

    componentDidMount () {
        console.log('<CicloVidaDemo> componentDidMount');
        // this.interval = setInterval(() => {
        //     console.log('go!')
        // }, 100);
    }

    componentWillReceiveProps (nextProps) {
        console.log('<CicloVidaDemo> componentWillReceiveProps', nextProps);
    }

    componentWillUpdate (nextProps, nextState) {
        console.log('<CicloVidaDemo> componentWillUpdate', nextProps, nextState);
    }

    shouldComponentUpdate (nextProps, nextState) {
        console.log('<CicloVidaDemo> shouldComponentUpdate', nextProps, nextState);
        console.log('actualProps', this.props.sizeMessage);
        console.log('nextProps', nextProps.sizeMessage);
        console.log('actualState', this.state.mensaje);
        console.log('nextState', nextState.mensaje);

        // Solo renderiza si han cambiado las propiedades
        //return this.props.sizeMessage !== nextProps.sizeMessage;
        return this.props.sizeMessage !== nextProps.sizeMessage || this.state.mensaje !== nextState.mensaje;
    }

    componentDidUpdate (prevProps, prevState) {
        console.log('<CicloVidaDemo> componentDidUpdate', prevProps, prevState);
    }

    componentWillUnmount () {
        console.log('<CicloVidaDemo> componentWillUnmount');
    }

    // Metodos
    changeState () {
        this.setState({ mensaje: 'Mensaje actualizado!' })
        // nunca se cambia un estado directamente. this.state es inmutable
        //this.state = { mensaje: 'nuevo mensaje' };
    }

    render () {
        //return <p>Hello world</p>;
        console.log('<CicloVidaDemo> render')
        return (
            <div>
                <h1>Demo de ciclo de vida</h1>
                <p><strong>State: </strong><code>{JSON.stringify(this.state)}</code></p>
                <p><strong>Props: </strong><code>{JSON.stringify(this.props)}</code></p>
                <button onClick={this.changeState}>Cambiar estado del componente</button>
            </div>
        );
    }
}