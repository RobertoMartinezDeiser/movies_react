import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Spinner from 'react-spinkit'
import './formularioBusqueda.css'

export default class FormularioBusqueda extends Component {

    constructor (...args) {
        super(...args);

        this.state = {
            textToSearch: '',
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (e) {
        this.setState({ textToSearch: e.currentTarget.value });
        // setState es asincrono, si queremos leer del estado nada mas actualizarlo, debemos hacerlo
        // desde su callback
        // this.setState({ textToSearch: e.currentTarget.value }, () => {
        //     console.log('Texto', this.state.textToSearch);
        // });
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.onSubmit(this.state.textToSearch);

        // TODO Variar el texto de busqueda tras una busqueda
        //this.setState({ textToSearch: '' })
        //event.target.value = ''; // cuando el eventhandler era handleTituloKeyPress
    }

    render () {
        return (
            <form className='Searcher' onSubmit={this.handleSubmit}>
                <input
                    disabled={this.props.isLoading}
                    onChange={this.handleChange}
                    type='text' />
                <button>{
                    this.props.isLoading
                        ? <Spinner name="circle" fadeIn="none" />
                        : 'Buscar'
                }</button>
            </form>
        );
    }
}

FormularioBusqueda.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func,
};

FormularioBusqueda.defaultProps = {
    onSubmit: () => { console.log('Submit sin implementar del formulario de busqueda') }
}