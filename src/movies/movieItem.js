import React from 'react'
import './main.css';

export default function MovieItem (props) {
    return (
        <li>
			<a href={'http://www.imdb.com/title/' +  props.imdb} target='_blank' rel='noopener noreferrer'>
				<div className='superior'>
					<label className='titulo'>{ props.titulo }</label>
					<label className='anyo'>{ props.year }</label>
				</div>
				<div className='inferior'>
                    <img className='calidad' src={props.calidad === 'HD' ? 'hd.png': 'sd.png'} width='25px' alt='imagenCalidad' />
					<label className='audio'>{ props.audio }</label>
				</div>
			</a>
        </li>
    )
}