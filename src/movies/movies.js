import React, { Component } from 'react';
import FormularioBusqueda from './formularioBusqueda.js';
import MovieRepository from '../repositories/movie.js';
import MovieItem from './movieItem';
import './main.css';

const repository = new MovieRepository();

function List(props) {
    return (
        <div id='listaPeliculas'>
            {/* {props.children && <span>Number of items: {props.children.length}</span>} */}
            <ul className='list'>{props.children}</ul>
        </div>
    )
}

export default class Movies extends Component {

    constructor (...args) {
        super(...args);

        this.state = {
            data: [],
            isLoading: false,
            message: null,
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        // console.log('filtro: ' + this.props.filtro);
        // console.log('recientes: ' + this.props.recientes);

        if (this.props.recientes === true) {
            repository.getDataFromDbRecientes()
                .then(resultado => this.setState({ data: resultado.data }));
        }
        // TODO si quitamos esto no cargaremos peliculas hasta que filtremos
        // else if (this.props.filtro === undefined) {
        //     repository.getDataFromDbPeliculas()
        //         .then(resultado => this.setState({ data: resultado.data }));
        // } else {
        //     repository.getDataFromDbPeliculasFiltro(this.props.filtro)
        //         .then(resultado => this.setState({ data: resultado.data }));
        // }
    }

    handleSubmit (textToSearch) {
        // TODO El isLoading no está funcionando, tiene pinta que este método termina instantaneamente
        // pero repository.getDataFromDbPeliculasFiltro tarda más en ejecutarse (¿asincrono?).
        this.setState({ isLoading: true })

        // console.log('handleSubmit: ' + textToSearch);
        repository.getDataFromDbPeliculasFiltro(textToSearch)
            .then(resultado => this.setState({ data: resultado.data }));

        this.setState({ isLoading: false })
    }

    render() {
        const { data } = this.state;
        // console.log(data);

        return (
            <div>
                <h1 id='textoContenido'>Lista de películas</h1>

                {(this.props.recientes === false) && <FormularioBusqueda onSubmit={this.handleSubmit} isLoading={this.state.isLoading}/>}

                <List>
                    {/* TODO Mejorar la linea que muestra el total de elementos de la lista */}
                    {data.length > 0 ? 'Películas en la lista: ' + data.length : ''}

                    {data.length <= 0
                        ? ''
                        : data.map((row) => (
                            <MovieItem key={row.idMovie} {...row} />
                        ))
                    }
                </List>
            </div>
        );
    }
}