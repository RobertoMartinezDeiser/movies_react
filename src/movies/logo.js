import React from 'react'

export default function Logo (props) {
    return (
        <div align="middle" style={{ MarginTop: '20px' }}>
            <img src='Kodi_logo.png' width="60%" alt="Kodi logo" />
        </div>
    )
}