import React, { Component } from 'react';
import MovieRepository from '../repositories/movie.js';

const repository = new MovieRepository();

export default class About extends Component {
    constructor(...args) {
        super(...args);

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        repository.getStats()
            .then(resultado => this.setState({ data: resultado }));
    }

    render() {
        const{ data } = this.state;
        //console.log(data);

        return (
            <div>
                <h1 id='textoContenido'>Acerca de</h1>
                <h3>Películas SD: {data.sd}</h3>
                <h3>Películas HD: {data.hd}</h3>
                <h3>Total Películas : {data.total}</h3>
            </div>
        );
    }
}