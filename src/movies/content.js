import React, { Component } from 'react';
import Movies from './movies';
import About from './about';

export default class Content extends Component {
    render() {
        return (
            <div>
                {(this.props.demoToLoad === 'MoviesRecientes') && <Movies recientes={true} />}
                {(this.props.demoToLoad === 'Movies') && <Movies recientes={false} />}
                {(this.props.demoToLoad === 'About') && <About />}
            </div>
        );
    }
}