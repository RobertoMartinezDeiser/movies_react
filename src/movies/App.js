import React, { Component } from 'react';
import Navbar from './navbar';
import Content from './content';
import './main.css';

export default class App extends Component {
    constructor(...args) {
        super(...args);

        this.changeDemo = this.changeDemo.bind(this);

        this.state = {
            demoToLoad: 'MoviesRecientes'
        }
    }

    changeDemo(demoToLoad) {
        return () => { // arrow function to bind this context
            this.setState({ demoToLoad });
        }
    }

    render() {
        const { demoToLoad } = this.state;

        return (
            <div id="container">
                <div id="navbar"><Navbar demoToLoad={demoToLoad} onClick={(demo) => this.changeDemo(demo)} /></div>
                <div id="content"><Content demoToLoad={demoToLoad} /></div>
            </div>
        )
    }
}