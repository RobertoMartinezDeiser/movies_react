import React, { Component } from 'react';
import Logo from './logo.js';
import './main.css';

export default class Navbar extends Component {
    render() {
        return (
            <div>
                <Logo />
                <div id="listaNavegacion">
                    <ul>
                        <li>
                            <button
                                className={this.props.demoToLoad === 'MoviesRecientes' ? 'activo' : ''}
                                onClick={this.props.onClick('MoviesRecientes')}>Recientes</button>
                        </li>
                        <li>
                            <button
                                className={this.props.demoToLoad === 'Movies' ? 'activo' : ''}
                                onClick={this.props.onClick('Movies')}>Películas</button>
                        </li>
                        <li>
                            <button
                                className={this.props.demoToLoad === 'About' ? 'activo' : ''}
                                onClick={this.props.onClick('About')}>Acerca de</button>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}