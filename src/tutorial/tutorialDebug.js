import React from 'react';
import ReactDOM from 'react-dom';

class ConsoleLogButton extends React.Component {
    onClick() {
        //https://developers.google.com/web/tools/chrome-devtools/console/console-write?hl=es
        console.log('Mensaje de %clog desde react', 'color:blue');
    }

    render() {
        return <button onClick={this.onClick}>Log de consola simple</button>
    }
}

class ConsoleTableButton extends React.Component {
    onClick() {
        console.table([{
            curso: 'ReactJs',
            duracion: 20,
        }, {
            curso: 'NodeJs',
            duracion: 30,
        }]);
    }

    render() {
        return <button onClick={this.onClick}>Log en tabla</button>
    }
}

class ConsoleTraceButton extends React.Component {
    onClick() {
        console.trace();
    }

    render() {
        return <button onClick={this.onClick}>Log de traza</button>
    }
}

class DebuggerButton extends React.Component {
    onClick() {
        debugger;
    }

    render() {
        return <button onClick={this.onClick}>Debugger</button>
    }
}

//export default ConsoleLogButton;

ReactDOM.render(
    <div>
        <ConsoleLogButton />
        <br/>
        <ConsoleTableButton />
        <br/>
        <ConsoleTraceButton />
        <br/>
        <DebuggerButton />
    </div>,
    document.getElementById('root')
)